import { Component, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-chatbot-menu',
  templateUrl: './chatbot-menu.component.html',
  styleUrls: ['./chatbot-menu.component.css']
})
export class ChatbotMenuComponent implements OnInit {
  public eventEmitter: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onClickPrintChatConversation(){
    console.log(this.eventEmitter);
    this.eventEmitter.emit({msg: "printChat"});
  }
  onClickRecentHistory(){    
    this.eventEmitter.emit({msg: "recentHistory"});
  }
}
