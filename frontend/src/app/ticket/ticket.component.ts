import { Component, OnInit } from '@angular/core';
import { DbService } from '../service/db.service';
import { Papa } from 'ngx-papaparse';
import * as saveAs from 'file-saver';
import { TrainingData } from '../models/TrainingData.model';
import { Misc } from '../service/Misc';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AddTrainingDataComponent } from '../add-training-data/add-training-data.component';
import { Ticket } from '../models/Ticket.model';
import { CreateTicketComponent } from './create-ticket/create-ticket.component';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {


  ticketsArr = [];

  //datepicker
  searchStartTimeString: any;
  searchEndTimeString: any;

  constructor(private _dbservice: DbService, private _papaParser: Papa, private modalService: BsModalService) { }

  ngOnInit() {
    this.resetHtml();
    this.populateTicketsArr();
    let htmlDate = new Date(Date.now());
    console.log(htmlDate);
  }


  resetHtml() {
    console.log("Resetting HTML..");
    this.searchStartTimeString = null;
    this.searchEndTimeString = null;
  }


  populateDummyTicketData() {
    let ticket: Ticket;
    for (let k = 1; k <= 5; k++) {
      ticket = new Ticket();
      ticket.issue_id = "" + k;
      ticket.issue_key = "key-" + k;
      ticket.issue_type = "type-" + k;
      ticket.assignee = "Assignee-" + k;
      ticket.creator = "Crator-" + k;
      ticket.priority = "Priority-" + k;
      ticket.project_key = "Project Key-" + k;
      ticket.project_lead = "Project Lead-" + k;
      ticket.reporter = "Reporter-" + k;
      ticket.resolution = "Resolution-" + k;
      ticket.description = "Description-" + k;
      this.ticketsArr.push(ticket);
    }
  }

  async populateTicketsArr() {
    this.resetHtml();
    this.populateDummyTicketData();
    // this._dbservice.getAllTrainingData().subscribe(
    //   data => {
    //     console.log("Got Training data from db... ");
    //     console.log(data);
    //     this.trainingDataArr = data['data'];
    //   },
    //   error => {
    //     console.log("error....");
    //     console.log(error);
    //   }
    // )
  }

  //search 
  onClickSearchItems() {
  //   console.log("feedback: " + this.searchFeedbackType);
  //   console.log("answer: " + this.searchAnswerType);
  //   console.log("searchStartTimeString: " + this.searchStartTimeString);
  //   console.log("searchEndTimeString: " + this.searchEndTimeString);


  //   //check the search params
  //   var searchStartTimeNumber = (this.searchStartTimeString == null || this.searchStartTimeString == undefined) ? -1 : Misc.getTimeStamp(this.searchStartTimeString);
  //   var searchEndTimeNumber = (this.searchEndTimeString == null || this.searchEndTimeString == undefined) ? -1 : Misc.getTimeStamp(this.searchEndTimeString);
  //   console.log("searchStartTimeNumber: " + searchStartTimeNumber);
  //   console.log("searchEndTimeNumber: " + searchEndTimeNumber);

  //   var searchFeedbackTypeNumber = -1;
  //   if (this.searchFeedbackType == "Like") searchFeedbackTypeNumber = 1;
  //   if (this.searchFeedbackType == "Dislike") searchFeedbackTypeNumber = 0;
  //   var searchAnswerTypeNumber = -1;
  //   if (this.searchAnswerType == "Answered") searchAnswerTypeNumber = 1;
  //   if (this.searchAnswerType == "Unanswered") searchAnswerTypeNumber = 0;

  //   //modify search start and end time number
  //   if (searchStartTimeNumber != -1) searchStartTimeNumber = Misc.initSearchStartTime(searchStartTimeNumber);
  //   if (searchEndTimeNumber != -1) searchEndTimeNumber = Misc.initSearchEndTime(searchEndTimeNumber);

  //   this._dbservice.getSpecificTrainingData(searchStartTimeNumber, searchEndTimeNumber, searchFeedbackTypeNumber, searchAnswerTypeNumber).subscribe(
  //     data => {
  //       console.log("success: data=");
  //       console.log(data);
  //       this.trainingDataArr = data['data'];
  //     },
  //     err => {
  //       console.log("Error:");
  //       console.log(err);
  //     }
  //   )
  // }

  // //delete training data
  // async onClickDeleteTrainingData(event) {
  //   console.log("Training Data to be deleted: " + event.target.value);
  //   var result = confirm("You really want to delete?");
  //   if (result) {
  //     this._dbservice.deleteTrainingData({ _id: event.target.value }).subscribe(
  //       data => {
  //         console.log("Training Data deleted. Now fetching fresh data : " + data);
  //         this.populatetrainingDataArr();
  //       },
  //       err => {
  //         console.log("error: ");
  //         console.log(err);
  //       }
  //     )
  //   } else {
  //     console.log("nnnnnnnnnnnn");
  //   }

    
  }


  onClickReset() {
    this.resetHtml();
  }

  // //download training data to csv file
  // onClickDownloadTrainingData() {
  //   console.log("about to save file ....");
  //   var fileContents = this._papaParser.unparse(this.getDownloadSpecificRecords());
  //   var file = new Blob([fileContents], { type: "text/plain;charset=utf-8" });
  //   let fileNameToSave = "dashboard_logs.csv";
  //   saveAs(file, fileNameToSave);
  // }

  // getDownloadSpecificRecords() {
  //   // let tmpItemLogsArr = this.itemLogsArr;
  //   let tmpTrainingDataArr = [];
  //   for (let k = 0; k < this.trainingDataArr.length; k++) {
  //     var x = {
  //       "log_date": new Date(this.trainingDataArr[k].log_date),
  //       "question": this.trainingDataArr[k].question,
  //       "answer": this.trainingDataArr[k].answer,
  //       "feedback": this.trainingDataArr[k].feedback,
  //       "sme": this.trainingDataArr[k].sme
  //     }
  //     tmpTrainingDataArr.push(x);
  //   }
  //   return tmpTrainingDataArr;
  // }

  //on clickeing the pencil update 
  onClickUpdatePencil(trainingData) {
    console.log("This log will be updated... ");
    console.log(trainingData);
  }

  // modifyTrainingData(event) {
  //   //call to http service
  //   console.log('clicked... : ' + event);
  //   console.log(this.trainingDataArr);
  //   this.isTrainingDataUpdated = true;
  // }

  // onClickSaveRetrain() {
  //   // alert("This functionality is yet to be implemented");
  //   this._dbservice.updateManyTrainingData(this.trainingDataArr).subscribe(
  //     data => {
  //       console.log(data);
  //       this.populatetrainingDataArr().then(reply => {
  //         alert("Training Data will get uploaded now.");
  //       });
  //     },
  //     err => {
  //       console.log(err);
  //       alert("Error in uploading Data");
  //     }
  //   )
  // }

  bsModalRef: BsModalRef;
  openModalWithComponent() {
    const initialState = {
      list: [
      ],
      title: 'Create New Ticket'
    };
    this.bsModalRef = this.modalService.show(CreateTicketComponent, { initialState });
    console.log("dashboard: bsmodal:");
    console.log(this.bsModalRef);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.bsModalRef.content.event.subscribe(data => {
      console.log('Child component\'s event was triggered', data);
      this.bsModalRef.hide();
      if (data.msg == "add")
        // this.populatetrainingDataArr();
        this.populateDummyTicketData();
    });
  }


  //File Upload
  ///////////////////////////////////////////////////////////
  //upload training data
  currentUploadFile = null;
  onClickUploadFile(event: any) {
    if (event.target.files && event.target.files.length > 0) {
      this.currentUploadFile = event.target.files[0];
    }
  }

  async onClickUploadTrainingData() {
    if (this.currentUploadFile == null) {
      alert("Kindly first choose the file to upload.");
      return;
    }

    await this.parseCSVAndAddTrainingData(this.currentUploadFile);
    // await this.parseAndTrain(this.currentUploadFile);

    alert("Training Data successfully uploaded.");
  }


  /**
   * Parse and add Incentive csv file with following conitions on the file and is using PapaParser    
  */
  async parseCSVAndAddTrainingData(csvFileToParse: File) {
    console.log("parseCSV. Ready to parse file: " + csvFileToParse.name);

    let options = {
      complete: (results, file) => {
        // console.log('Parsed: ', results, file);
        // console.log("File:");
        // console.log(file);
        console.log("Results:");
        console.log(results);
        console.log(results.data.length);

        let tmpTrainingDataArr: Array<TrainingData> = [];
        let lineNo = 0;
        for (let res of results.data) {
          console.log("RES:" + lineNo);
          console.log(res);
          //ignore the first line as it would be heading          
          lineNo++;
          if (lineNo == 1) continue;//this is the header so ingnore it


          //if answer is not availbale for a particular question then skip such rows
          console.log("2nd res: " + res[1]);
          if (res[1] == null || res[1] == "") {
            console.log("Skipping question: " + res[1]);
            continue;
          }

          //create incentive objects
          let trainingData = new TrainingData();
          trainingData.question = res[0];
          trainingData.answer = res[1];
          if (res[2] != null && res[2] != "") trainingData.feedback = res[2];
          if (res[3] != null && res[3] != "") trainingData.sme = res[3];
          if (res[4] != null && res[4] != "") trainingData.is_answered = res[4].toLowerCase() == 'true' ? true : false;
          if (res[5] != null && res[5] != "") trainingData.is_liked = res[5].toLowerCase() == 'true' ? true : false;
          tmpTrainingDataArr.push(trainingData);
        }

        //print all incentive
        // console.log("All item logs.... ")
        // console.log(tmpTrainingDataArr);

        this._dbservice.createTrainingDataFromUpload(tmpTrainingDataArr).subscribe(
          data => {
            console.log("Got reply from server.... .... ");
            console.log(data);
            // ///////////////this.populatetrainingDataArr();
          },
          err => {
            console.log("Got error.... " + err);
          }
        );


      },
      // Add other options here
      skipEmptyLines: true
    };
    try {
      this._papaParser.parse(csvFileToParse, options);
      console.log("Success in parsing.");
    } catch (error) {
      console.log("Error in parsing....")
      console.log(error);
    }

  }

}//end of class

