import { Component, OnInit } from '@angular/core';
import { ItemLog } from '../models/ItemLog.model';
import { DbService } from '../service/db.service';
import { Papa } from 'ngx-papaparse';
import * as saveAs from 'file-saver';
import { Misc } from '../service/Misc';

@Component({
  selector: 'app-item-logs-dashboard',
  templateUrl: './item-logs-dashboard.component.html',
  styleUrls: ['./item-logs-dashboard.component.css']
})
export class ItemLogsDashboardComponent implements OnInit {

  itemLogsArr = [];

  //flags
  isItemLogUpdated = false;

  //html specific 
  searchFeedbackType = "All";
  searchAnswerType = "All";

  //datepicker
  searchStartTimeString: any;
  searchEndTimeString: any;

  constructor(private _dbservice: DbService, private _papaParser: Papa) { }

  ngOnInit() {
    this.populateItemLogsArr();
    let htmlDate = new Date(Date.now());
    console.log(htmlDate);
  }

  resetHtml() {
    console.log("Resetting HTML..");
    this.searchStartTimeString = null;
    this.searchEndTimeString = null;
    this.searchFeedbackType = "All";
    this.searchAnswerType = "All";
    this.isItemLogUpdated = false;
  }

  async populateItemLogsArr() {
    this._dbservice.getAllItemLogs().subscribe(
      data => {
        console.log("Got item logs from database: ");
        console.log(data);
        this.itemLogsArr = data['data'];
        // for(let k=0; k<this.itemLogsArr.length; k++){
        //   if(this.itemLogsArr[k].is_liked) this.itemLogsArr[k].feedback = "Liked";
        //   else if(!this.itemLogsArr[k].is_liked) this.itemLogsArr[k].feedback = "Disliked";
        // }
      },
      error => {
        console.log("error....");
        console.log(error);
      }

    )
  }

  //update item log
  onClickUpdateItemLog(event) {
    console.log("This item log will be updated: " + event.target.value);
  }

  //
  modifyItemLog(event) {
    //call to http service
    console.log(this.itemLogsArr);
    this.isItemLogUpdated = true;
  }

  //delete item log
  async onClickDeleteItemLog(event) {
    console.log("Item log will be deleted: " + event.target.value);
    var result = confirm("You really want to delete?");
    if (result) {
      this._dbservice.deleteItemLog({ _id: event.target.value }).subscribe(
        data => {
          console.log("Item deleted. Now fetching fresh item logs: " + data);
          this.populateItemLogsArr();
        },
        err => {
          console.log("error: ");
          console.log(err);
        }
      )
    }
  }


  getDownloadSpecificRecords(){
    // let tmpItemLogsArr = this.itemLogsArr;
    let tmpItemLogsArr = [];
    for (let k = 0; k < this.itemLogsArr.length; k++) {
      var x = {
        "log_date" : new Date(this.itemLogsArr[k].log_date),
        "question": this.itemLogsArr[k].question,
        "answer": this.itemLogsArr[k].answer,
        "feedback": this.itemLogsArr[k].feedback,
        "sme": this.itemLogsArr[k].sme
      }
      // console.log("x:::");
      // console.log(x);
      tmpItemLogsArr.push(x);
    }
    return tmpItemLogsArr;
  }

  //download item logs to csv file
  onClickDownloadItemLogs() {
    console.log("about to save file ....");
    // console.log(this.itemLogsArr);
    //modify log dates
    var fileContents = this._papaParser.unparse(this.getDownloadSpecificRecords());
    var file = new Blob([fileContents], { type: "text/plain;charset=utf-8" });
    let fileNameToSave = "logs.csv";
    saveAs(file, fileNameToSave);
  }


  onClickSaveRetrain() {
    // alert("This functionality is yet to be implemented");
    this._dbservice.updateManyItemLogs(this.itemLogsArr).subscribe(
      data => {
        console.log(data);
        this.populateItemLogsArr();
        alert("Training Data succesfully updated.");
      },
      err => {
        console.log(err);
      }
    )
  }//end of class



  //search 
  onClickSearchItems() {
    console.log("feedback: " + this.searchFeedbackType);
    console.log("answer: " + this.searchAnswerType);
    console.log("searchStartTimeString: " + this.searchStartTimeString);
    console.log("searchEndTimeString: " + this.searchEndTimeString);


    //check the search params
    var searchStartTimeNumber = (this.searchStartTimeString == null || this.searchStartTimeString == undefined) ? -1 : Misc.getTimeStamp(this.searchStartTimeString);
    var searchEndTimeNumber = (this.searchEndTimeString == null || this.searchEndTimeString == undefined) ? -1 : Misc.getTimeStamp(this.searchEndTimeString);
    console.log("searchStartTimeNumber: " + searchStartTimeNumber);
    console.log("searchEndTimeNumber: " + searchEndTimeNumber);

    var searchFeedbackTypeNumber = -1;
    if (this.searchFeedbackType == "Like") searchFeedbackTypeNumber = 1;
    if (this.searchFeedbackType == "Dislike") searchFeedbackTypeNumber = 0;
    var searchAnswerTypeNumber = -1;
    if (this.searchAnswerType == "Answered") searchAnswerTypeNumber = 1;
    if (this.searchAnswerType == "Unanswered") searchAnswerTypeNumber = 0;

    //modify search start and end time number
    if(searchStartTimeNumber != -1) searchStartTimeNumber = Misc.initSearchStartTime(searchStartTimeNumber);
    if(searchEndTimeNumber != -1) searchEndTimeNumber = Misc.initSearchEndTime(searchEndTimeNumber);

    this._dbservice.getSpecificItemLogs(searchStartTimeNumber, searchEndTimeNumber, searchFeedbackTypeNumber, searchAnswerTypeNumber).subscribe(
      data => {
        console.log("success: data=");
        console.log(data);
        this.itemLogsArr = data['data'];
      },
      err => {
        console.log("Error:");
        console.log(err);
      }
    )
  }

  onClickReset() {
    this.resetHtml();
  }

  //File Upload
  ///////////////////////////////////////////////////////////
  //upload item logs
  currentUploadFile = null;
  onClickUploadFile(event: any) {
    if (event.target.files && event.target.files.length > 0) {
      this.currentUploadFile = event.target.files[0];
      // let fileName = file.name;

    }
  }
  async onClickUploadItemLogs() {
    if (this.currentUploadFile == null) {
      alert("Kindly first choose the file to upload.");
      return;
    }

    await this.parseCSVAndAddItemLogs(this.currentUploadFile);
    alert("item Logs successfully uploaded.");
  }



  /**
   * Parse and add Incentive csv file with following conitions on the file and is using PapaParser    
  */
  async parseCSVAndAddItemLogs(csvFileToParse: File) {
    console.log("parseCSV. Ready to parse file: " + csvFileToParse.name);

    let options = {
      complete: (results, file) => {
        console.log("Results:");
        console.log(results);
        console.log(results.data.length);

        let tmpItemLogsArr: Array<ItemLog> = [];
        let lineNo = 0;
        for (let res of results.data) {
          console.log("RES:" + lineNo);
          console.log(res);
          //ignore the first line as it would be heading          
          lineNo++;
          if (lineNo == 1) continue;//this is the header so ingnore it

          //create incentive objects
          let itemLog = new ItemLog();
          itemLog.question = res[0];
          itemLog.answer = res[1];
          itemLog.feedback = res[2];
          itemLog.sme = res[3];
          itemLog.is_answered = res[4];
          itemLog.is_liked = res[5];
          tmpItemLogsArr.push(itemLog);
        }

        this._dbservice.createItemLogsFromUpload(tmpItemLogsArr).subscribe(
          data => {
            console.log("Got reply from server.... .... ");
            console.log(data);
            this.populateItemLogsArr();
          },
          err => {
            console.log("Got error.... " + err);
          }
        );


      },
      // Add other options here
      skipEmptyLines: true
    };
    try {
      this._papaParser.parse(csvFileToParse, options);
      console.log("Success in parsing.");
    } catch (error) {
      console.log("Error in parsing....")
      console.log(error);
    }

  }


}//end of class
