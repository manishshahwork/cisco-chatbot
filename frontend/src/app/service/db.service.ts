import { Injectable } from '@angular/core';
import { ItemLog } from '../models/ItemLog.model';
import { HttpClient } from '@angular/common/http';
import { TrainingData } from '../models/TrainingData.model';

@Injectable({
    providedIn: 'root'
})
export class DbService {

    // HOST_URL = "http://ec2-13-233-148-63.ap-south-1.compute.amazonaws.com:3000";
    HOST_URL = "http://localhost:3000";
    constructor(private http: HttpClient) { }

    ///////////////////////// Item Log Specific
    //get all item logs
    getAllItemLogs() {
        return this.http.get(this.HOST_URL + "/itemLogs" + "/getAll");
    }

    //delete item log
    deleteItemLog(itemLogId) {
        let url = this.HOST_URL + "/itemLogs" + "/deleteOne";
        console.log("url: " + url);
        return this.http.post(url, itemLogId);
    }

    addItemLog(itemLog){
        let url = this.HOST_URL + "/itemLogs" + "/add";
        console.log("url: " + url);
        return this.http.post(url, itemLog);
    }

    createItemLogsFromUpload(itemLogsArr: ItemLog[]) {
        let url = this.HOST_URL + "/itemLogs" + "/addMany";
        console.log("url: " + url);
        return this.http.post(url, itemLogsArr);
    }

    updateManyItemLogs(itemLogsArr: ItemLog[]) {
        let url = this.HOST_URL + "/itemLogs" + "/updateManyItemLogs";
        console.log("url: " + url);
        return this.http.post(url, itemLogsArr);
    }

    setLikeDislikeForItemLog(itemLogId, isLike) {
        let url = this.HOST_URL + "/itemLogs" + "/setLikeDislike";
        console.log("url: " + url + " itemLogId:" + itemLogId + " like:" + isLike);
        return this.http.post(url, {itemLogId, isLike});
    }

    
    //get specific item log data
    getSpecificItemLogs(searchStartTime, searchEndTime, searchFeedbackType, searchAnswerType) {
        let url = this.HOST_URL + "/itemLogs/search" + "/" + searchStartTime + "/" + searchEndTime + "/" + searchFeedbackType + "/" + searchAnswerType;
        console.log("url: " + url);
        return this.http.get(url);
    }

    ///////////////////////// Training Data Specific
    //get all training data
    getAllTrainingData() {
        return this.http.get(this.HOST_URL + "/trainingData" + "/getAll");
    }

    createTrainingDataFromUpload(trainingDataArr: TrainingData[]) {
        let url = this.HOST_URL + "/trainingData" + "/addMany";
        console.log("url: " + url);
        return this.http.post(url, trainingDataArr);
    }

    //get specific training data
    getSpecificTrainingData(searchStartTime, searchEndTime, searchFeedbackType, searchAnswerType) {
        let url = this.HOST_URL + "/trainingData/search" + "/" + searchStartTime + "/" + searchEndTime + "/" + searchFeedbackType + "/" + searchAnswerType;
        console.log("url: " + url);
        return this.http.get(url);
    }

    updateManyTrainingData(trainingDataArr: TrainingData[]) {
        let url = this.HOST_URL + "/trainingData" + "/updateManyTrainingData";
        console.log("url: " + url);
        return this.http.post(url, trainingDataArr);
    }

     //delete item log
     deleteTrainingData(trainingDataId) {
        let url = this.HOST_URL + "/trainingData" + "/deleteOne";
        console.log("url: " + url);
        return this.http.post(url, trainingDataId);
    }

    getMatchingAnswers(question){
        return this.http.get(this.HOST_URL + "/trainingData" + "/getMatchingAnswers" + "/" + question);
    }

    addTrainingData(trainingData: TrainingData){
        let url = this.HOST_URL + "/trainingData" + "/add";
        console.log("url: " + url);
        return this.http.post(url, trainingData);
    }

    // /////////////
    // dummyItemLog() {
    //     let itemLog = new ItemLog();
    //     itemLog.log_date = Date.now();
    //     itemLog.question = "how are you?";
    //     itemLog.answer = "Great...";
    //     itemLog.sme = "sme";
    //     itemLog.feedback = "good one.... ";

    //     return itemLog;
    // }
}
